import React from 'react'
import ReactDOM from "react-dom";
import QRCode from "react-qr-code";

function Qr() {
    return (
        <div>
            <QRCode value="https://python.org" />
        </div>
    )
}

export default Qr

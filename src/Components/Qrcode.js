import React, { useState, useEffect } from 'react'
import axios from 'axios'
import scannerimg from '../Components/Images/qrcode.png'
import logo from '../Components/Images/logo.png'
import '../Components/qrcode.css'
import ReactDOM from "react-dom";
import QRCode from "react-qr-code";

function Qrcode() {

    
    const [num, setnum] = useState('')
    const [list, setList] = useState([])
    const [showQr, setShowQr] = useState(false)
    let selectedvalue = (event) => {
        setnum(event.target.value)
        setShowQr(!showQr)
        if (showQr === true) {
            setShowQr(showQr)
        }
    }
    useEffect(async () => {
       
        await axios.get(`http://localhost:3100/checkin/getofficeadd`)
          .then(officeAdd => {{var respData = officeAdd.data.data.map((item) => { return item.OfficeAddress })
                setList(respData) } }) }, [])

    return (
        <div className='main-div'>
            <div className='parent-div'>
                <div className='org-logo'><img src={logo} alt='organization-log' /></div>
                <div>
                    <p className='office-add'>Select office address</p>
                    <div className='select-city '>
                        <select className='select-box' value={num} onChange={selectedvalue}>
                            <option value="" defaultValue disabled hidden>Select city</option>
                            {
                                list.map((ofcadd,index) => {
                                   return <option key={index} value={ofcadd} >{ofcadd}</option>
                                })
                            }
                        </select>
                    </div>
                </div>
                {showQr ? <div className='content-div'>
                    <div className='ccc' id='qrcode'>
                        <div className='scanner-img-div'>
                            {/* <img src={scannerimg} alt='qr-scanner-img' /> */}
                            <QRCode className='hello' value={`${num}`} />
                        </div>
                        <p className='check-in'>Check-In</p>
                    </div>

                </div> : null}
            </div>
        </div>
    )
}

export default Qrcode
